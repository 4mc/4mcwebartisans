$(document).ready(function() {

    //Plugin para los slides
    $('#slides').slides({
        preloadImage : 'images/loading.gif',
        effect : 'slide, fade',
        generateNextPrev : true,
        generatePagination : true
    });
    
    $("#accordion").accordion();
 
    //Cambiando el nombre de los links de generateNextPrev
    $(".prev").html("Anterior");
    $(".next").html("Siguiente");
    
    $(".ocultar").hide();    
        
    
    //Eventos en los slides
    $(".necesito li").click(function() {
        $(this).css({
            color : '#657C9C',
            cursor : 'pointer',
            'list-style-image' : 'url(images/button_ok.jpg)'
        });

    });

    $("#ventajas").click(function() {
        $("#verVentajas").show('slide');
    });

    $("#desventajas").click(function() {
        $("#verDesventajas").show('slide');
    });

    $("#hojaEstilos").click(function() {
        $("#verHojaEstilos").show('slide');
    });

    $("p.mostrar").click(function() {
        $(this).next('pre').show('');

    });

});
