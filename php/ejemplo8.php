<html>
<head>
<title>Ejemplo 8</title></head>
<body>
<?php

class rockstar{

	var $fans = 1;
	var $money = 1;

	function rockstar($name){
		$this->name = $name;
	}
		
	function moreFans($more){
		$this->fans+=$more;
	}
	
	function moreMoney($more = 1){
		
		$this->money+=$more;
	}
	
	function lessMoney($less = 1){
		
		$this->money-=$less;
	}
	
	function makeConcert($number =1, $ticket = 10){
		
		$this->money-=100;
		$this->money+=($ticket*$number);
		$this->money+=($number*.5);	
	}
	
	function makeAlbum($number =1, $album = 5){
		
		$this->money-=200;
		$this->money+=($album*$number);	
		$this->money+=($number*.4);
	}
	
}


$armin = new rockstar("Armin van Buuren");

print_r($armin);
echo "<br>";

$armin->moreMoney(1000);

print_r($armin);
echo "<br>";

$armin->makeConcert(500,20);

print_r($armin);
echo "<br>";

$armin->makeAlbum(1000,5);

print_r($armin);
echo "<br>";

?>
</body>
</html>