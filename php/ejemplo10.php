<html>
<head>
<title>Ejemplo 10</title></head>
<body>
<?php

class superheroe{

	var $superpower = true;
	var $clothing = "adjusted";
	var $realName = "secret";

	function superheroe($name){
		$this->name = $name;
	}
	
	function setRealName($realName){
		$this->realName=$realName;
	}
	
}


$spiderman = new superheroe("Spiderman");

echo "<br>";
print_r($spiderman);


class avenger extends superheroe{
	
	function avenger($name){
		$this->avenger=true;
		parent::superheroe($name);
	}
}

$iroman = new avenger("ironman");

echo "<br>";
print_r($iroman);

$iroman->realName = "Tony Stark";

echo "<br>";
print_r($iroman);




?>
</body>
</html>