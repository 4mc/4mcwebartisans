<html>
<head>
<title>Ejemplo 11</title></head>
<body>
<?php

class superheroe{

	public $superpower = true;
	private $clothing = "adjusted";
	protected $realName = "secret";

	function superheroe($name){
		$this->name = $name;
	}
	
	function setRealName($realName){
		$this->realName=$realName;
	}
	
}


$spiderman = new superheroe("Spiderman");

echo "<br>";
print_r($spiderman);


class avenger extends superheroe{
	
	function avenger($name){
		$this->avenger=true;
		parent::superheroe($name);
	}
}

$iroman = new avenger("ironman");

echo "<br>";
print_r($iroman);

$iroman->clothing = "Mark";

echo "<br>";
print_r($iroman);


$iroman->realName = "Tony Stark";

echo "<br>";
print_r($iroman);




?>
</body>
</html>