<html>
<head>
<title>Ejemplo 7</title></head>
<body>
<?php

class rockstar{

	var $fans = 1;
	var $money = 1;

	function rockstar($name){
		$this->name = $name;
	}
		
	function moreFans($more){
		$this->fans+=$more;
	}
	
	function moreMoney($more = 1){
		
		$this->money+=$more;
	}
}


$armin = new rockstar("Armin van Buuren");

print_r($armin);
echo "<br>";

$armin->moreFans(2);

print_r($armin);
echo "<br>";

$armin->moreMoney();

print_r($armin);
echo "<br>";

$armin->moreMoney(5);

print_r($armin);
echo "<br>";


?>
</body>
</html>