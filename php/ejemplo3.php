<html>
<head>
</head>
<body>
<?

$pepe="";


$luis="Me llamo Luis y soy serio y formal";
$luisa="Me llamo Luisa y tambi&eacute;n soy seria y formal";

?>

<!-- esto es HTML, hemos cerrado el script -->

<center><b>Vamos a ver el contenido de las variables</b></center>

<!--   un nuevo script PHP -->

<?
echo "<br> El valor de la variable $luis es: ".$luis;
echo "<br> El valor de la variable luis es: ".$luis;
?>

<center><b><br>Invocando la variable desde una funci&oacute;n</b></center>

<?php

function vervariable(){

echo "<br> Si invoco la variable Luis desde una funci&oacute;n";
echo "<br>me aparecer&aacute; en blanco";
echo "<br>El valor de la variable Luis es: ".$luis;
}

vervariable();
?>

<!-- mas HTML puro  -->
<center><b><br>Ver la variable desde la funci&oacute;n
                        poniendo <i>global</i></b></center>	

<?php
# una nueva funcion

function ahorasi(){
	
			global $luis;

   echo "<br><br> Hemos asignado &aacute;mbito global a la variable";
   echo "<br>ahora Luis aparecer&aacute;";
   echo "<br>El valor de la variable  Luis es: ".$luis;

}
# hemos cerrado ya la funcion con la llave. 
# Tendremos que invocarla para que se ejecute ahora
ahorasi();
?>

<center><b><br>Un solo nombre y dos <i>variables distintas</i></b><br>
Dentro de la funci&oacute;n el valor de la variable es <br></center>

<?php
function cambiaLuisa(){

	$luisa="Ahora voy a llamarme Ana por un ratito";
	
	echo "<br>".$luisa;
}

cambiaLuisa();
?>
<center>... pero despu&eacute;s de salir de la funci&oacute;n
                    vuelvo al valor original...</center>
<?
echo "<br>".$luisa;
?>


</body>
</html>