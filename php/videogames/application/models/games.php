<?
class Games extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function getGames(){
		$query = $this->db->get('games');
		return $query->result_array();
	}
	
	function getGamebyName($name){
		$query = $this->db->like('game', $name);
		$query = $this->db->get('games');
		return $query->result_array();
	}
	
}