<html>
<head>
<title>Ejemplo 12</title></head>
<body>
<?php

class superheroe{

	function __construct(){
	
		$this->superpower = true;
		$this->clothing = "adjusted";
		$this->realName = "secret";
	}

	function superheroe($name){
		$this->name = $name;
	}
	
	function setRealName($realName){
		$this->realName=$realName;
	}
	
}


$spiderman = new superheroe("Spiderman");

echo "<br> Spiderman: ";
print_r($spiderman);

$spiderman->name="Spiderman";	

echo "<br> Spiderman: ";
print_r($spiderman);

class avenger extends superheroe{
	
	function __construct(){
		parent::__construct();
	}
	
	function avenger($name){
		$this->avenger=true;
		parent::superheroe($name);
	}
}

$iroman = new avenger("ironman");

echo "<br> IronMan :";
print_r($iroman);

$iroman->realName = "Tony Stark";

echo "<br> IronMan : ";
print_r($iroman);




?>
</body>
</html>